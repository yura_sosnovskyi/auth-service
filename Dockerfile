FROM openjdk:11-jre-slim
WORKDIR /app
COPY target/auth-service-0.0.1-SNAPSHOT-spring-boot.jar /app/app.jar
EXPOSE 8081
CMD ["java", "-jar", "app.jar"]
