package com.sosnovskyi.authservice;

import com.github.javafaker.Faker;
import com.sosnovskyi.authservice.service.SpecialistAuthService;
import com.sosnovskyi.commondomain.dto.SpecialistInputDto;
import com.sosnovskyi.commondomain.model.EducationType;
import com.sosnovskyi.commondomain.model.Gender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "test.data", name = "seeder", havingValue = "true")
public class CommandLineDataSeeder implements CommandLineRunner {

    private final SpecialistAuthService specialistAuthService;
    private final Faker faker;
    private final List<Long> categoriesIds;

    public CommandLineDataSeeder(SpecialistAuthService specialistAuthService) {
        this.specialistAuthService = specialistAuthService;
        this.faker = new Faker();
        categoriesIds = List.of(2L, 3L, 4L, 5L,
                7L, 8L, 9L, 10L,
                12L, 13L, 14L, 15L, 16L, 17L,
                19L, 20L, 21L, 22L, 23L, 24L,
                26L, 27L, 28L, 29L, 30L, 31L);
    }

    @Override
    public void run(String... args) {
        for (int i = 0; i < 100; i++) {
            log.info("Saving " + i + ": " + generateSpecialistInputDto().toString());
            specialistAuthService.saveSpecialist(generateSpecialistInputDto());
        }
    }

    public SpecialistInputDto generateSpecialistInputDto() {
        return SpecialistInputDto.builder()
                .email(faker.internet().emailAddress())
                .firstname(faker.name().firstName())
                .lastname(faker.name().lastName())
                .phoneNumber(faker.phoneNumber().phoneNumber())
                .password("admin")
                .city(faker.address().city())
                .age(faker.number().numberBetween(18, 65))
                .gender(faker.options().option(Gender.class))
                .title(faker.job().title())
                .educationType(faker.options().option(EducationType.class))
                .educationInstitution(faker.university().name())
                .description(faker.job().keySkills() + ". "
                        + faker.job().position())
                .cardNumber(faker.finance().creditCard())
                .experience(faker.number().numberBetween(1, 40))
                .categoryIds(generateRandomCategoryIds())
                .build();
    }

    private List<Long> generateRandomCategoryIds() {
        int numCategories = faker.number().numberBetween(1, 6);
        List<Long> localCategoryIds = new ArrayList<>();
        for (int i = 0; i < numCategories; i++) {
            Long categoryId = categoriesIds.get(faker.number().numberBetween(0, categoriesIds.size()));
            localCategoryIds.add(categoryId);
        }
        return localCategoryIds;
    }
}
