package com.sosnovskyi.authservice.service;

import com.sosnovskyi.commondomain.dto.SpecialistInputDto;
import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.service.CommonSpecialistService;
import org.springframework.stereotype.Service;

@Service
public interface SpecialistAuthService extends CommonSpecialistService {
    SpecialistOutputDto saveSpecialist(SpecialistInputDto specialistInputDto);

}
