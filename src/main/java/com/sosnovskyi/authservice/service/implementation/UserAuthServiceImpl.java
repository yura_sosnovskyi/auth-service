package com.sosnovskyi.authservice.service.implementation;

import com.sosnovskyi.authservice.repository.RoleAuthRepository;
import com.sosnovskyi.authservice.repository.UserAuthRepository;
import com.sosnovskyi.authservice.service.UserAuthService;
import com.sosnovskyi.authservice.util.JwtUtil;
import com.sosnovskyi.commondomain.dto.UserInputDto;
import com.sosnovskyi.commondomain.dto.UserOutputDto;
import com.sosnovskyi.commondomain.exception.UserAlreadyExistsException;
import com.sosnovskyi.commondomain.exception.UserNotFoundException;
import com.sosnovskyi.commondomain.model.Role;
import com.sosnovskyi.commondomain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static com.sosnovskyi.commondomain.mapper.UserMapper.fromInputDto;
import static com.sosnovskyi.commondomain.mapper.UserMapper.toOutputDto;


@Service
public class UserAuthServiceImpl implements UserAuthService {

    private final UserAuthRepository userAuthRepository;
    private final RoleAuthRepository roleAuthRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtil;


    @Autowired
    public UserAuthServiceImpl(UserAuthRepository userAuthRepository,
                               RoleAuthRepository roleAuthRepository,
                               JwtUtil jwtUtil) {
        this.userAuthRepository = userAuthRepository;
        this.roleAuthRepository = roleAuthRepository;
        this.passwordEncoder = new BCryptPasswordEncoder(10);
        this.jwtUtil = jwtUtil;
    }

    @Override
    public UserOutputDto saveUser(UserInputDto userInputDto) {
        if (userAuthRepository.existsByEmail(userInputDto.getEmail())) {
            throw new UserAlreadyExistsException(userInputDto.getEmail());
        }
        User user = fromInputDto(userInputDto, Collections.singletonList(getOrCreateRole("USER")));
        user.setEncodedPassword(passwordEncoder.encode(userInputDto.getPassword()));
        return toOutputDto(userAuthRepository.save(user));
    }

    @Override
    public Map<String, String> generateTokenIfUserExists(UserInputDto userInputDto) {
        User userEntity = findUserByEmailAndPassword(userInputDto)
                .orElseThrow(() -> new UserNotFoundException(userInputDto.getEmail()));

        return jwtUtil.generateToken(userEntity);
    }

    @Override
    public UserOutputDto validateTokenForUser(String token) {
        String email = jwtUtil.extractUsername(token);
        User user = userAuthRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(email));
        return toOutputDto(user);
    }

    private Role getOrCreateRole(String roleName) {
        return roleAuthRepository.findByName(roleName)
                .orElse(new Role(roleName));
    }

    private Optional<User> findUserByEmailAndPassword(UserInputDto userInputDto) {
        return userAuthRepository.findByEmail(userInputDto.getEmail())
                .filter(userEntity ->
                        passwordEncoder.matches(userInputDto.getPassword(), userEntity.getEncodedPassword()));
    }
}
