package com.sosnovskyi.authservice.service.implementation;

import com.sosnovskyi.authservice.repository.RoleAuthRepository;
import com.sosnovskyi.authservice.repository.UserAuthRepository;
import com.sosnovskyi.authservice.service.SpecialistAuthService;
import com.sosnovskyi.commondomain.dto.SpecialistInputDto;
import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.exception.UserAlreadyExistsException;
import com.sosnovskyi.commondomain.model.Category;
import com.sosnovskyi.commondomain.model.Role;
import com.sosnovskyi.commondomain.model.Specialist;
import com.sosnovskyi.commondomain.repository.CommonCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.sosnovskyi.commondomain.mapper.SpecialistMapper.fromInputDto;
import static com.sosnovskyi.commondomain.mapper.SpecialistMapper.toOutputDto;


@Service
public class SpecialistAuthServiceImpl implements SpecialistAuthService {

    private final UserAuthRepository userAuthRepository;
    private final RoleAuthRepository roleAuthRepository;
    private final CommonCategoryRepository categoryRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SpecialistAuthServiceImpl(UserAuthRepository userAuthRepository,
                                     RoleAuthRepository roleAuthRepository,
                                     CommonCategoryRepository commonCategoryRepository) {
        this.userAuthRepository = userAuthRepository;
        this.roleAuthRepository = roleAuthRepository;
        this.categoryRepository = commonCategoryRepository;
        this.passwordEncoder = new BCryptPasswordEncoder(10);
    }

    @Override
    public SpecialistOutputDto saveSpecialist(SpecialistInputDto specialistInputDto) {
        if (userAuthRepository.existsByEmail(specialistInputDto.getEmail())) {
            throw new UserAlreadyExistsException(specialistInputDto.getEmail());
        }
        Specialist specialist = fromInputDto(specialistInputDto,
                List.of(getOrCreateRole("USER"),
                        getOrCreateRole("SPECIALIST")),
                getCategories(specialistInputDto.getCategoryIds()));
        specialist.setEncodedPassword(passwordEncoder.encode(specialistInputDto.getPassword()));
        return toOutputDto(userAuthRepository.save(specialist));
    }

    private List<Category> getCategories(List<Long> categoryIds) {
        return categoryIds.stream()
                .map(id -> categoryRepository.findById(id).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Role getOrCreateRole(String roleName) {
        return roleAuthRepository.findByName(roleName)
                .orElse(new Role(roleName));
    }
}
