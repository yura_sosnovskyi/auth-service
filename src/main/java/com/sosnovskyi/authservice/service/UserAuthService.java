package com.sosnovskyi.authservice.service;

import com.sosnovskyi.commondomain.dto.UserInputDto;
import com.sosnovskyi.commondomain.dto.UserOutputDto;
import com.sosnovskyi.commondomain.service.CommonUserService;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface UserAuthService extends CommonUserService {
    UserOutputDto saveUser(UserInputDto user);
    Map<String, String> generateTokenIfUserExists(UserInputDto userInputDto);
    UserOutputDto validateTokenForUser(String token);
}
