package com.sosnovskyi.authservice.repository;

import com.sosnovskyi.commondomain.model.User;
import com.sosnovskyi.commondomain.repository.CommonUserRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserAuthRepository extends CommonUserRepository {
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);
}
