package com.sosnovskyi.authservice.controller;

import com.sosnovskyi.authservice.service.SpecialistAuthService;
import com.sosnovskyi.authservice.service.UserAuthService;
import com.sosnovskyi.commondomain.dto.SpecialistInputDto;
import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.dto.UserInputDto;
import com.sosnovskyi.commondomain.dto.UserOutputDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("auth")
public class AuthController {
    private final UserAuthService userAuthService;
    private final SpecialistAuthService specialistAuthService;

    public AuthController(UserAuthService userAuthService,
                          SpecialistAuthService specialistAuthService) {
        this.userAuthService = userAuthService;
        this.specialistAuthService = specialistAuthService;
    }

    @PostMapping("/users/register")
    public ResponseEntity<UserOutputDto> registerNewUser(@RequestBody UserInputDto userInputDto) {
        return new ResponseEntity<>(userAuthService.saveUser(userInputDto), HttpStatus.CREATED);
    }

    @PostMapping("/specialists/register")
    public ResponseEntity<SpecialistOutputDto> registerNewSpecialist(@RequestBody SpecialistInputDto specialistInputDto) {
        return new ResponseEntity<>(specialistAuthService.saveSpecialist(specialistInputDto), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody UserInputDto userInputDto) {
        return new ResponseEntity<>(userAuthService.generateTokenIfUserExists(userInputDto), HttpStatus.OK);
    }

    @GetMapping("/token/validate")
    public ResponseEntity<UserOutputDto> validateToken(@RequestParam String token) {
        return new ResponseEntity<>(userAuthService.validateTokenForUser(token), HttpStatus.OK);
    }
}
