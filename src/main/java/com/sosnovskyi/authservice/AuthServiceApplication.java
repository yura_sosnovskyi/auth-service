package com.sosnovskyi.authservice;

import com.sosnovskyi.commondomain.exception.ErrorHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(
//        scanBasePackages = {
//                "com.sosnovskyi.commondomain",
//                "com.sosnovskyi.authservice"
//        },
        exclude = {
                SecurityAutoConfiguration.class
        }
)
@EntityScan(basePackages = "com.sosnovskyi.commondomain.model")
@EnableJpaRepositories(basePackages = {
        "com.sosnovskyi.authservice.repository",
        "com.sosnovskyi.commondomain.repository"
})
@EnableJpaAuditing
@Import(ErrorHandler.class)
public class AuthServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthServiceApplication.class, args);
    }
}
